import java.util.Date;
import java.util.List;
import java.util.TimerTask;

public class TimerJob extends TimerTask {
    private String name ;
    private String bankCodex;
    public TimerJob(String n,String code){
        this.name=n;
        this.bankCodex = code;
    }

    @Override
    public void run() {

        System.out.println(Thread.currentThread().getName()+" "+name+" the task has executed successfully "+ new Date());
        if("task1".equalsIgnoreCase(name)){
            try {
                List<FileDto> readStatus = ReadDataCSV.readStatus(bankCodex);
                System.out.println("Selamat Siang Rekan ");
                for (FileDto data : readStatus) {
                    System.out.println("-Envi "+data.getEnvi()+" Port "+data.getPort()+" terpantau offline");
                }
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


}
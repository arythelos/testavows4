import java.io.*;
import java.util.*;

public class ReadFileServerStatus {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter Bank Code");
        String bankCode = myObj.nextLine();
        System.out.println("bankCode is: " + bankCode);
        Timer timer = new Timer();
        TimerTask task = new TimerJob("task1",bankCode);
        timer.scheduleAtFixedRate(task, new Date(), 10000);//ini 10 detik kalau mau 1 hari tinggal diganti 86400000
        System.out.println("Time is :"+task.scheduledExecutionTime());
    }

}

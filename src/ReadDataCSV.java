import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ReadDataCSV {
    static List<FileDto> readStatus(String inputan){
        List<FileDto> fileDtos = new ArrayList<>();
        BufferedReader readData = null;
        String line = "";
        String files = "input/test.csv";
        try {
            FileReader fileReader = new FileReader(files);
            readData = new BufferedReader(fileReader);
            line = readData.readLine();
            readData.lines().forEach(linex->{
            if(linex.trim() == "" || linex.isEmpty() || linex.split(";").length <= 0)
                System.out.println("baris kosong");
            else{
                System.out.println(linex.trim().split(";"));
                    System.out.println("kode tidak sesuai");
                    String[] attributes = linex.split(";");
                    if (attributes[0].equalsIgnoreCase(inputan)) {
                        FileDto datas = createDataFromFile(attributes);
                        fileDtos.add(datas);
                    } else {
                        System.out.println("kode tidak sesuai");
                    }
                }
            });
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return fileDtos;
    }

    private static FileDto createDataFromFile(String[] dataFrom) {
        String bankCode = dataFrom[0];
        String envi = dataFrom[1];
        String port = dataFrom[2];
        String bankName = dataFrom[3];

        // create and return book of this metadata
        return new FileDto(bankCode, bankName, envi, port);
    }
}

public class FileDto {
    private String bankCode;
    private String bankName;
    private String envi;
    private String port;

    public FileDto(String bankCode, String bankName, String envi, String port) {
        this.bankCode = bankCode;
        this.bankName = bankName;
        this.envi = envi;
        this.port = port;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getEnvi() {
        return envi;
    }

    public void setEnvi(String envi) {
        this.envi = envi;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
}
